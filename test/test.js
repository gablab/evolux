var assert = require('chai').assert;

const GAME_CONSTANTS = require('../lux/game_constants');

const behavior = require('../behavior/behavior.js');
const Decision = behavior.Decision;
const Sense = behavior.Sense;

const act_functions = require('../behavior/act_functions.js');
const sense_functions = require('../behavior/sense_functions.js');

// https://stackoverflow.com/a/9050354/5599687
if(!Array.prototype.last){
    Array.prototype.last = function() {
        return this[this.length - 1];
    };
};


describe('Sense', function() {
  var sense;
  var actor = {};
  var gameMap = {};
  beforeEach(function() {
    actor.cargoSpaceLeft = () => {return 10;};
    gameMap.height = 9;
    gameMap.width = 6;

    sense = new Sense();
    sense.add_observable( (actor, gameMap, player, opponent) => { return { // get cargo space left
      'actor_cargo_space_left': actor.cargoSpaceLeft()
    }; });
    sense.add_observable( (actor, gameMap, player, opponent) => { return { // get map area
      'map_area': gameMap.height*gameMap.width       
    }; });
  });

  describe('observe()', function() {
    it('should sense according to the provided function', function() {
      sensed = sense.observe(actor, gameMap, {}, {});
      assert.equal(sensed.actor_cargo_space_left, 10);
      assert.equal(sensed.map_area, 54);
    });
  });
});

describe('Decision', function() {
  var decision;
  var sense;
  beforeEach(function() {
    decision = new Decision();
    decision.set_threshold_condition('observable', 3, true);
    decision.set_equality_condition('observable', 7, false);
    sense = new Sense();
    sense.observable = 5;
  });

  describe('set_existence_condition()', function() {
    it('should allow imposing a necessary condition', function() {
      var old_length = decision.conditions_necessary.length;
      decision.set_existence_condition('observable', true);
      assert.lengthOf(decision.conditions_necessary, old_length+1);
      assert.isTrue(decision.conditions_necessary.last().evaluate(sense));
    });
    it('should allow imposing an optional condition', function() {
      var old_length = decision.conditions_optional.length;
      decision.set_existence_condition('asdfg', false);
      assert.lengthOf(decision.conditions_optional, old_length+1);
      assert.isFalse(decision.conditions_optional.last().evaluate(sense));
    });
  });
  describe('set_threshold_condition()', function() {
    it('should allow imposing a necessary condition', function() {
      var old_length = decision.conditions_necessary.length;
      decision.set_threshold_condition('observable', 3, true);
      assert.lengthOf(decision.conditions_necessary, old_length+1);
      assert.isTrue(decision.conditions_necessary.last().evaluate(sense));
    });
    it('should allow imposing an optional condition', function() {
      var old_length = decision.conditions_optional.length;
      decision.set_threshold_condition('observable', 7, false);
      assert.lengthOf(decision.conditions_optional, old_length+1);
      assert.isFalse(decision.conditions_optional.last().evaluate(sense));
    });
  });
  describe('set_equality_condition()', function() {
    it('should allow imposing a necessary condition', function() {
      var old_length = decision.conditions_necessary.length;
      decision.set_equality_condition('observable', 3, true);
      assert.lengthOf(decision.conditions_necessary, old_length+1);
      assert.isFalse(decision.conditions_necessary.last().evaluate(sense));
    });
    it('should allow imposing an optional condition', function() {
      var old_length = decision.conditions_optional.length;
      decision.set_equality_condition('observable', 5, false);
      assert.lengthOf(decision.conditions_optional, old_length+1);
      assert.isTrue(decision.conditions_optional.last().evaluate(sense));
    });
  });
  describe('evaluate()', function() {
    it('should compute and return a score based on conditions', function() {
      assert.equal(decision.evaluate(sense), 0.5);
    });
    it('should return 0 for a false necessary condition', function() {
      decision.set_equality_condition('observable', 3, true);
      assert.equal(decision.evaluate(sense), 0);
    });
  });

  describe('queue_act()', function() {
    it('should allow insertion of a function', function() {
      var old_length = decision.acts.length;
      decision.queue_act((decision, actor)=>{return;});
      assert.lengthOf(decision.acts, old_length+1);
    });
  });
  describe('execute()', function() {
    it('should execute the acts in order', function() {
      var actor = {'property': 5}
      decision.queue_act((decision, actor)=>{actor.property += 2;});
      decision.queue_act((decision, actor)=>{actor.property *= actor.property;});
      decision.actuate(actor);
      assert.equal(actor.property, 49);
    })
  });
});

function make_fake_cell(resource_type, this_pos) {
  return {
    'resource': {
      'type': resource_type,
    },
    'hasResource': () => { return resource_type === undefined ? false : true; },
    'pos': {
      'x': this_pos[0],
      'y': this_pos[1],
      'distanceTo': (other_pos) => {
        dist_x = other_pos[0] - this_pos[0];
        dist_y = other_pos[1] - this_pos[1];
        return Math.sqrt(dist_x*dist_x + dist_y*dist_y);
      }
    }
  };
};

describe('act functions', function() {
  describe('generate_move_towards_tile()', function() {
    it('should generate a function that moves the unit towards a tile', function() {
      var move_toward_tileX = act_functions.generate_move_towards_tile('tileX');
      var decision = new Decision();
      decision.tileX = make_fake_cell('coal', [1, 1]);
      var unit_coords = [0, 0];
      var unit = {
        pos: {
          directionTo: (pos_to)=>{ return [1, 0]; }
        },
        move: (direction)=>{
          return [unit_coords[0]+direction[0], unit_coords[1]+direction[1]];
        }
      };
      //assert.equal(move_toward_tileX(decision, unit), [1, 0]);
      decision.queue_act(move_toward_tileX);
      decision.actuate(unit);
      var last_action = decision.result_actions.last();
      // the presence of the `last` method makes equality fail
      assert.equal(last_action[0], 1);
      assert.equal(last_action[1], 0);
    });
  });
});

describe('sense_functions', function() {
  var actor = {};
  var gameMap = {};
  var player = {};

  beforeEach(function() {
    //GAME_CONSTANTS = { 'RESOURCE_TYPES': { 'COAL': 'coal', 'URANIUM': 'uranium' } };
    actor.cargoSpaceLeft = () => { return 10; };
    actor.pos = [2, 2];
    // only wood tiles are valid
    player.researchedCoal = ()=>false;
    player.researchedUranium = ()=>false;
    /* gameMap schema: coords are x=column, y=row
     *  ____
     * |CU.|    // C, U, W are the resources initial letters
     * |...|
     * |W.X|    // X is the actor/unit -> expected nearest resource is (3, 2) (only wood)
     * |.WC|
     *  ----
     */
    let coal = GAME_CONSTANTS.RESOURCE_TYPES.COAL;
    let uranium = GAME_CONSTANTS.RESOURCE_TYPES.URANIUM;
    let wood = GAME_CONSTANTS.RESOURCE_TYPES.WOOD;
    gameMap.tiles = [
      [[coal, [0, 0]], [undefined, [0, 1]], [wood, [0, 2]], [undefined, [0, 3]]], // column 0
      [[uranium, [1, 0]], [undefined, [1, 1]], [undefined, [1, 2]], [wood, [1, 3]]], // column 1
      [[undefined, [2, 0]], [undefined, [2, 1]], [undefined, [2, 2]], [coal, [2, 3]]], // column 2
    ].map((row)=>row.map((x)=>make_fake_cell(...x)));
    gameMap.height = gameMap.tiles[0].length;
    gameMap.width = gameMap.tiles.length;
    gameMap.getCell = (x, y)=>gameMap.tiles[x][y];
  });

  describe('get_cargo_space_left()', function() {
    it('should return the cargo space left for the passed actor', function() {
      assert.equal(sense_functions.get_cargo_space_left(actor, gameMap, {}, {}).actor_cargo_space_left, 10);
    });
  });
  
  describe('get_closest_usable_resource_tile()', function() {
    it('should return the distance from the nearest resource and the tile itself', function() {
      // test the mock first
      assert.equal(gameMap.height, 4);
      assert.equal(gameMap.width, 3);
      for (let y = 0; y < gameMap.height; y++) {
        for (let x = 0; x < gameMap.width; x++) {
          const cell = gameMap.getCell(x, y);
          assert.isNotNull(cell);
        }
      }
      assert.equal(gameMap.getCell(0, 0).pos.distanceTo([1, 0]), 1);
      assert.equal(gameMap.getCell(0, 0).pos.distanceTo([1, 1]), Math.sqrt(2));
      assert.equal(gameMap.getCell(2, 3).resource.type, GAME_CONSTANTS.RESOURCE_TYPES.COAL);
      assert.isTrue(gameMap.getCell(2, 3).hasResource());

      sensed = sense_functions.get_closest_usable_resource_tile(actor, gameMap, player, {});
      assert.equal(sensed.closest_resource_distance, Math.sqrt(2));
      assert.equal(sensed.closest_resource_tile, gameMap.tiles[1][3]);
    });
  });
});



