/** generate an act function that moves towards a tile
 *  parameters:
 *  - name_of_tile: the name of the property of the Decision object that
 *                  represents the destination tile
 */
const generate_move_towards_tile = (name_of_tile) => {
  const generated_move_toward_tile = (decision, actor) => {
    var unit = actor;
    var closestResourceTile = decision[name_of_tile];
    const dir = unit.pos.directionTo(closestResourceTile.pos);
    decision.result_actions.push(unit.move(dir)); // this will be passed up to the game/simulation
  };
  return generated_move_toward_tile;
};


module.exports = {
  generate_move_towards_tile
}
