/** Represents a condition that influences a decision. */
class Condition {
  variable_name;
  target_value;
  type;
  multiplicator = 1;

  /** creates a condition depending on type:
   *    'threshold' -> variable_name*multiplicator > target_value
   *    'equality'  -> variable_name*multiplicator == target_value // multiplicator only for consistency
   */
  constructor(variable_name, target_value, type, multiplicator=1) {
    this.variable_name = variable_name;
    this.target_value = target_value;
    if (! type in ['threshold', 'equality', 'existence']) throw `Condition constructor: type is not valid (passed ${type})`;
    this.type = type;
    this.multiplicator = multiplicator;
  }

  /** Evaluates if the condition is true or false, depending on the variable_name value 
   *    in the passed `sensed` object. 
   */
  evaluate(sensed) {
    var actual_value = sensed[this.variable_name];
    if (!actual_value) return false;
    if (this.type == 'threshold')
      return actual_value*this.multiplicator > this.target_value;
    else if (this.type == 'equality')
      return actual_value*this.multiplicator == this.target_value;
    else if (this.type == 'existence')
      return actual_value != undefined;
    else
      throw `Condition.evaluate: type is not valid (${this.type})`;
  }
}


/** Represents a decision: depending on sensed conditions, some acts are made. */
class Decision {

  conditions_necessary = [];
  conditions_optional = [];
  /** add an existence condition to the decision (see Condition) */
  set_existence_condition(variable_name, is_necessary=False) {
    var condition = new Condition(variable_name, undefined, 'existence');
    if (is_necessary)
      this.conditions_necessary.push(condition);
    else
      this.conditions_optional.push(condition);
  }
  /** add a threshold condition to the decision (see Condition) */
  set_threshold_condition(variable_name, target_value, is_necessary=false, multiplicator=1) {
    var condition = new Condition(variable_name, target_value, 'threshold', multiplicator=multiplicator);
    if (is_necessary)
      this.conditions_necessary.push(condition);
    else
      this.conditions_optional.push(condition);
  }
  /** add an equality condition to the decision (see Condition) */
  set_equality_condition(variable_name, target_value, is_necessary=false) {
    var condition = new Condition(variable_name, target_value, 'equality');
    if (is_necessary)
      this.conditions_necessary.push(condition);
    else
      this.conditions_optional.push(condition);
  }

  /** give a score to the decision based on sensed conditions */
  evaluate(senses){
    if (this.conditions_necessary.length == 0 && this.conditions_optional.length == 0) return 1;
    for (var cond of this.conditions_necessary) {
      if (!cond.evaluate(senses)) return 0; // if at least one necessary condition is false
    }
    var count = 0;
    for (var cond of this.conditions_optional) {
      if (cond.evaluate(senses)) count++;
    }
    return (count+this.conditions_necessary.length)/(this.conditions_optional.length+this.conditions_necessary.length);
  }


  /** the sequence of acts to be done */
  acts = [];

  /** add an act to the sequence; it should be a function that accepts as parameters:
   *    - decision: `this` object
   *    - actor: an unit of the game/simulation
   */
  queue_act(act_function) {
    this.acts.push(act_function);
  }

  /** the sequence of actions to be passed to the game/simulation */
  result_actions = [];
  
  /** execute all the acts in order; the actor executing is passed to the function.
   * Returns the sequence of actions to be passed to the game/simulation.
   */
  actuate(actor) {
    for (let act of this.acts) {
      act(this, actor);
    }
    return this.result_actions;
  }
}

class Sense {
  observables = [];

  /** add an observable to be sensed;
   *  it should be a function that
   *  - accepts as parameters
   *    - `actor`, an unit of the game/simulation
   *    - `gameMap`, the Map of the game/simulation
   *    - `player`, the player of the game/simulation
   *    - `opponent`, the enemy player
   *  - returns an object with the measures
   */
  add_observable(sense_function) {
    this.observables.push(sense_function);
  }

  /** sense the functions in this moment */
  observe(actor, gameMap, player, opponent) {
    var sensed = {};
    for (let sense_function of this.observables) {
      var sensed_values = sense_function(actor, gameMap, player, opponent);
      for (let key in sensed_values) {
        sensed[key] = sensed_values[key];
      }
    }
    return sensed;
  }
}


module.exports = {
  Decision,
  Sense
}
