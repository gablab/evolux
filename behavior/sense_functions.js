const GAME_CONSTANTS = require('../lux/game_constants');
const DIRECTIONS = GAME_CONSTANTS.DIRECTIONS;

const get_cargo_space_left = (actor, gameMap, player, opponent) => { 
  return {
    'actor_cargo_space_left': actor.cargoSpaceLeft()
  }
};

const get_closest_usable_resource_tile = (actor, gameMap, player, opponent) => {
  let closestResourceTile = null;
  let closestDist = 9999999;
  for (let y = 0; y < gameMap.height; y++) {
    for (let x = 0; x < gameMap.width; x++) {
      const cell = gameMap.getCell(x, y);
      if (cell.hasResource()) {
        if (cell.resource.type === GAME_CONSTANTS.RESOURCE_TYPES.COAL && !player.researchedCoal()) continue;
        if (cell.resource.type === GAME_CONSTANTS.RESOURCE_TYPES.URANIUM && !player.researchedUranium()) continue;
        const dist = cell.pos.distanceTo(actor.pos);
        if (dist < closestDist) {
          closestDist = dist;
          closestResourceTile = cell;
        }
      }
    }
  }
  return {
    'closest_resource_distance': closestDist,
    'closest_resource_tile': closestResourceTile
  };
};


module.exports = {
  get_cargo_space_left,
  get_closest_usable_resource_tile
}
